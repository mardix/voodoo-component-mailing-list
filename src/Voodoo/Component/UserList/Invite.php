<?php
/**
 * Voodoo\Component\UserList\Invite
 * 
 * @name UserList
 * @author Mardix
 * @since   Feb 12, 2014
 * 
 * A component to create an invi
 */

namespace Voodoo\Component\UserList;

use Voodoo;

abstract class Invite extends Base
{
    protected $tableName = "invite_list";
    
    protected $codeSize = 12;
    
    public function findByCode($inviteCode) 
    {
        return $this->reset()->where("invite_code", $inviteCode)->findOne();
    }
    
    /**
     * Invite a 
     * @param type $to_email
     * @param type $from_email
     * @param type $from_id
     * @return string - the invite code
     * @throws Exception
     */
    public function invite($to_email, $from_email = null, $from_id = null)
    {
        $this->checkEmail($to_email);
        if (!$this->reset()->where("to_email", $to_email)->findOne()) {
            $inviteCode = $this->generateInviteCode();
            $data = [
                "to_email" => $to_email,
                "from_id" => $from_id,
                "from_email" => $from_email,
                "invite_sent" => 0,
                "invite_close" => 0,
                "invite_code" => $inviteCode 
            ];
            return $this->insert($data);
        } else {
            throw new Exception("Invitee has been invited already");
        }
    }

    /**
     * Return the invite code
     * @return string
     */
    public function getInviteCode()
    {
        if ($this->isSingleRow()) {
            return $this->invite_code;
        }
        return null;
    }
    
    /**
     * To close an invite
     * 
     * @param int $to_id
     * @return \Voodoo\Component\Invite
     * @throws Exception
     */
    public function closeInvite($to_id = null)
    {
        if ($this->isSingleRow()) {
            $this->update([
                "invite_close" => 1,
                "to_id" => $to_id,
                "closed_at" => self::NOW()
            ]);
        }
        return $this;
    }
    
    /**
     * Set invite as set
     * @return \Voodoo\Component\Invite
     */
    public function setInviteSent()
    {
        if ($this->isSingleRow()) {
            $this->update(["invite_sent" => 1]);
        }
        return $this;
    }
    
    /**
     * 
     * @return bool
     */
    public function isClosed()
    {
        if ($this->isSingleRow()) {
            return $this->invite_close ? true : false;
        }
        return false;
    }
    
    /**
     * 
     * @return bool
     */
    public function isSent()
    {
        if ($this->isSingleRow()) {
            return $this->invite_sent ? true : false;
        }
        return false;
    }
    
    
    /**
     * Generate an invite code
     * 
     * @return String
     */
    protected function generateInviteCode()
    {
        do {
            $inviteCode = Voodoo\Core\Helpers::generateRandomString($this->codeSize);
            $inviteCode = strtolower($inviteCode);
        } while ($this->findByCode($inviteCode));
        return $inviteCode;
    }


    private function checkEmail($email) 
    {
        if (! Voodoo\Core\Helpers::validEmail($email)) {
            throw new Exception("Invalid email address: $email");
        }
    }
    
/*******************************************************************************/    
    protected function setupTable()
    {
        $sql = "
            CREATE TABLE `{$this->getTableName()}` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `from_id` INT (10) UNSIGNED NULL DEFAULT NULL,
                `to_id` INT (10) UNSIGNED NULL DEFAULT NULL,
                `from_email` VARCHAR(125) NULL DEFAULT NULL,
                `to_email` VARCHAR(125) NOT NULL,
                `invite_code` VARCHAR(125) NOT NULL,
                `invite_close` TINYINT(1) NOT NULL DEFAULT '0',
                `invite_sent` TINYINT(1) NOT NULL DEFAULT '0',
                `display` TINYINT(1) NOT NULL DEFAULT '1',
                `closed_at` DATETIME NULL DEFAULT NULL,
                `created_at` DATETIME NULL DEFAULT NULL,
                `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                INDEX `to_email` (`to_email`)
            )
            COLLATE='utf8_bin'
            ENGINE=InnoDB;
        "; 
        $this->createTable($sql);            
    }
}
