<?php
/**
 * Voodoo\Component\UserList\MailingList
 * 
 * @name UserList
 * @author Mardix
 * @since   Feb 12, 2014
 * 
 * A component to create add people to a mailing list
 */

namespace Voodoo\Component\UserList;

use Voodoo;

class MailingList extends Base
{
    protected $tableName = "mailing_list";

    /**
     * To subscribe on a list
     * 
     * @param type $email
     * @param type $list
     * @param type $name
     * @return bool
     */
    public function subscribe($email, $list = "", $name = "")
    {
        $email = strtolower($email);
        $this->checkEmail($email);
        
        $subs = $this->reset()->where([
            "email" => $email,
            "list" => $list
        ])->findOne();
        
        if (! $subs) {
            $this->insert([
                "email" => $email,
                "name" => $name,
                "list" => $list,
            ]);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * To unsubscribe
     * 
     * @param type $email
     * @param type $list
     * @return boolean
     */
    public function unsubscribe($email, $list = "")
    {
        $email = strtolower($email);
        $this->checkEmail($email);
        
        $subs = $this->reset()->where([
            "email" => $email,
            "list" => $list
        ])->findOne();
        
        if ($subs) {
            $subs->update(["unsubscribe" => 1]);
            return true;
        } else {
            return false;
        }
    }
    
    private function checkEmail($email) 
    {
        if (! Voodoo\Core\Helpers::validEmail($email)) {
            throw new \Exception("Invalid email address: $email");
        }
    }
    
/*******************************************************************************/    
    protected function setupTable()
    {
        $sql = "
            CREATE TABLE `{$this->getTableName()}` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `email` VARCHAR(125) NOT NULL,
                `name` VARCHAR(125) NOT NULL,
                `list` VARCHAR(125) NOT NULL,
                `unsubscribe` TINYINT(1) NOT NULL DEFAULT '0',
                `display` TINYINT(1) NOT NULL DEFAULT '1',
                `created_at` DATETIME NULL DEFAULT NULL,
                `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                INDEX `email` (`email`)
            )
            COLLATE='utf8_bin'
            ENGINE=InnoDB;
        "; 
        $this->createTable($sql);            
    }
}
